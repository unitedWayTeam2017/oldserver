package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Options;

import com.uwde.dao.Event;

@Mapper
public interface EventMapper {

    @Select("SELECT * FROM Event")
    List<Event> getAllEvents();

    @Select("SELECT * FROM Event WHERE id = #{id}")
	List<Event> getEventById(int id);

  @Insert("INSERT into event(name, startdate, enddate, descr, street1, street2, city, state, zipCode, longitude, latitude) VALUES(#{name}, #{startDate}, #{endDate}, #{descr}, #{street1}, #{street2}, #{city}, #{state}, #{zipCode}, #{longitude}, #{latitude})")
  @Options(useGeneratedKeys=true, keyProperty="id")
  void insertEvent(Event event);

  @Update("UPDATE event set name=#{name}, startdate=#{startDate}, enddate=#{endDate}, descr=#{descr}, street1=#{street1}, street2=#{street2}, city=#{city}, state=#{state}, zipCode=#{zipCode}, longitude=#{longitude}, latitude=#{latitude} where id=#{id}")
  @Options(useGeneratedKeys=true, keyProperty="id")
  void updateEvent(Event event);

}
