package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;


import com.uwde.dao.EventVolunteer;

@Mapper
public interface EventVolunteerMapper {


  @Insert("INSERT into eventvolunteer(name,email,eventid) VALUES(#{name}, #{email}, #{eventId})")
  @Options(useGeneratedKeys=true, keyProperty="id")
  void insertVolunteer(EventVolunteer volunteer);

	//@Select("SELECT * FROM donation_category")
  //List<DonationCategory> getAllDonationCategories();

  @Select("SELECT * FROM eventvolunteer WHERE eventId = #{eventId}")
	List<EventVolunteer> getVolunteerByEventId(int eventId);

}
