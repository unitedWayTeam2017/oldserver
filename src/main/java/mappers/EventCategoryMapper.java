package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.uwde.dao.EventCategory;

@Mapper
public interface EventCategoryMapper {
	
	@Select("SELECT * FROM Event_Category")
    List<EventCategory> getAllEventCategories();

    @Select("SELECT * FROM Event_Category WHERE id = #{id}")
	List<EventCategory> getEventCategoryById(int id);

}
