package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;


import com.uwde.dao.Donation;

@Mapper
public interface DonationMapper {


  @Insert("INSERT into donation(name,email,amount,category) VALUES(#{name}, #{email}, #{amount}, #{category})")
  @Options(useGeneratedKeys=true, keyProperty="id")
  void insertDonation(Donation donation);

	//@Select("SELECT * FROM donation_category")
  //List<DonationCategory> getAllDonationCategories();

  //@Select("SELECT * FROM donation_category WHERE id = #{id}")
	//List<DonationCategory> getDonationCategoryById(int id);

}
