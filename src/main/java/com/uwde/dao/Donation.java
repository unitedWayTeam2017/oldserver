package com.uwde.dao;
import java.sql.Timestamp;

public class Donation {

	private int id;
	private String name;
	private String email;
  private String category;
  private Float amount;
	private Timestamp date;

	public Donation(){}
  public Donation(String name, String email, String category, Float amount) {
    this.name = name;
    this.email = email;
    this.amount = amount;
    this.category = category;
  }
	public String toString() {
		return name + " : " + email + " : " + amount + " : " +category;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
  public Float getAmount() {
    return amount;
  }
  public void setAmount(Float amount) {
    this.amount = amount;
  }
  public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
}
