package com.uwde.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.uwde.dao", "com.uwde.controller", "com.uwde.beans"})
public class App {

    public static void main(String[] args) {

        SpringApplication.run(App.class, args);
    }
}
