package com.uwde.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.uwde.dao.Event;

import mappers.EventMapper;

@RestController
@RequestMapping("/event")
public class EventController {

	@Autowired
	EventMapper eventMapper;

	@RequestMapping(method = {RequestMethod.GET})
    public List<Event> getAllEvents() {
		return eventMapper.getAllEvents();
    }

	@RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public List<Event> getEventById(@PathVariable int id) {
		return eventMapper.getEventById(id);
    }

		@RequestMapping(value="/insert", method = {RequestMethod.PUT})
	  @ResponseBody
	  public Event insertEvent(@RequestBody Event event) {
	    System.out.println("In insert controller: " + event);
	    eventMapper.insertEvent(event);
	    // System.out.println("Out controller");
	    return event;
	  }

		@RequestMapping(value="/update", method = {RequestMethod.PUT})
	  @ResponseBody
	  public Event updateEvent(@RequestBody Event event) {
	    System.out.println("In update controller: " + event);
	    eventMapper.updateEvent(event);
	    // System.out.println("Out controller");
	    return event;
	  }

}
