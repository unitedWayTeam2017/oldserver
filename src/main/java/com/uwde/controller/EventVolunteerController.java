package com.uwde.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.bind.annotation.RestController;

import com.uwde.dao.EventVolunteer;

import mappers.EventVolunteerMapper;

@RestController
@RequestMapping("/volunteer")
public class EventVolunteerController {

	@Autowired
	EventVolunteerMapper eventVolunteerMapper;

  @RequestMapping(value = "/insert", method = {RequestMethod.PUT})
  @ResponseBody
  public EventVolunteer insertVolunteer(@RequestBody EventVolunteer volunteer) {
    System.out.println("In insertVolunteer controller: " + volunteer);
    eventVolunteerMapper.insertVolunteer(volunteer);
    // System.out.println("Out controller");
    return volunteer;
  }

  @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public List<EventVolunteer> getVolunteerByEventId(@PathVariable int id) {
		return eventVolunteerMapper.getVolunteerByEventId(id);
    }
}
