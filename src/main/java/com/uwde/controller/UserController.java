package com.uwde.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.uwde.dao.User;

import mappers.UserMapper;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserMapper userMapper;
	
	@RequestMapping(method = {RequestMethod.GET})
    public List<User> getAllUsers() {
		return userMapper.getAllUsers();
    }
	
	@RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public List<User> getUserById(@PathVariable int id) {
		return userMapper.getUserById(id);
    }
	
}
