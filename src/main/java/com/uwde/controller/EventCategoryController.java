package com.uwde.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.uwde.dao.EventCategory;

import mappers.EventCategoryMapper;

@RestController
@RequestMapping("/eventCategory")
public class EventCategoryController {

	@Autowired
	EventCategoryMapper eventCategoryMapper;
	
	@RequestMapping(method = {RequestMethod.GET})
    public List<EventCategory> getAllEventCategories() {
		return eventCategoryMapper.getAllEventCategories();
    }
	
	@RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public List<EventCategory> getEventCategoryById(@PathVariable int id) {
		return eventCategoryMapper.getEventCategoryById(id);
    }
	
}
