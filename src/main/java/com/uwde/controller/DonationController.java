package com.uwde.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.bind.annotation.RestController;

import com.uwde.dao.Donation;

import mappers.DonationMapper;

@RestController
@RequestMapping("/donation")
public class DonationController {

	@Autowired
	DonationMapper donationMapper;

  @RequestMapping(method = {RequestMethod.PUT})
  @ResponseBody
  public Donation insertDonation(@RequestBody Donation donation) {
    System.out.println("In controller: " + donation);
    donationMapper.insertDonation(donation);
    // System.out.println("Out controller");
    return donation;
  }

}
