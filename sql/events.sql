create table event (
	id SERIAL primary key,
	name varchar(255) not null,
	startDate timestamp with time zone not null,
	endDate timestamp with time zone not null,
	descr varchar(1024) default '',
	street1 varchar(255),
  street2 varchar(255),
  city varchar(255),
  state varchar(255),
  zipCode varchar(10),
  latitude numeric,
  longitude numeric
);

insert into event (name, startdate, enddate, descr, street1, street2, city, state, zipCode, longitude, latitude) values
('Women United Networking and Art Exhibit', (timestamp '2017-05-23 08:00:00'), (timestamp '2017-05-23 21:00:00'), '', '20 Orchard Rd','','Newark','DE','19716',-75.7567428, 39.6806881),
('Sneaker Ball', (timestamp '2017-04-08 08:00:00'), (timestamp '2017-04-08 21:00:00'), '','5517 Limeric Circle','Apt 24','Wilmington','DE','19808', -75.5498539, 39.7466083);
