create table eventvolunteer (
	id SERIAL primary key,
	name varchar(255) not null,
	email varchar(255) not null,
	eventid numeric not null
);
